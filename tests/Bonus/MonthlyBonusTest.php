<?php

namespace App\Tests\Bonus;


use App\Entity\User;
use App\Service\BonusService;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


/**
 * Created by PhpStorm.
 * User: Misu
 * Date: 12/5/2020
 * Time: 9:09 AM
 */
final class MonthlyBonusTest extends WebTestCase
{

    private $bonusService;

    protected function setUp()/* The :void return type declaration that should be here would cause a BC issue */
    {
        self::bootKernel();
        $container = self::$kernel->getContainer();
        $this->bonusService = $container->get('bonus.service');
    }


    public function testMonthlyBonus()
    {
        /** @var BonusService $bonusService */
        $bonusService = $this->bonusService;
        $user = $bonusService->usefullServices->getEntityManager()->find(User::class,104);
        $circle = $bonusService->setBonusCircles($user);
        $monthlyBonus = $bonusService->getMonthlyBonus($circle,$user);
        $this->assertEquals(36, $monthlyBonus);
    }
}