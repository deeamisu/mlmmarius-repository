<?php
require_once __DIR__ . '/vendor/autoload.php';
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

$connection = new AMQPStreamConnection('127.0.0.1', 18038, 'admin', 'daniel123','ioptimizer');
$channel = $connection->channel();

$msg = new AMQPMessage('Hello World!');
$channel->basic_publish($msg, 'task', '');