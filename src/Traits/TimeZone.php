<?php
/**
 * Created by PhpStorm.
 * User: Misu
 * Date: 11/29/2020
 * Time: 4:56 AM
 */

namespace App\Traits;

use DateTime;
use Exception;

trait TimeZone
{
    /**
     * @param string $date
     * @return string
     * @throws Exception
     */
    public function getMonthYear(string $date): string
    {
        date_default_timezone_set('Europe/Bucharest');
        setlocale(LC_ALL, 'ro_RO.utf8');
        $monthYear = new DateTime($date);
        $currentMonthYear = strftime('%B %Y', strtotime($monthYear->format('d-m-Y')));
        return ucfirst($currentMonthYear);
    }

    /**
     * @param $date
     * @return string
     * @throws Exception
     */
    public function getYearMonth($date): string
    {
        date_default_timezone_set('Europe/Bucharest');
        setlocale(LC_ALL, 'ro_RO.utf8');
        $monthYear = new DateTime($date);
        return strftime('%Y - %m - %d', strtotime($monthYear->format('d-m-Y')));
    }
}