<?php

namespace App\Command;

use App\Entity\Subscription;
use App\Entity\User;
use App\Service\BonusService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class HistorySubscriptionCommand extends Command
{
    protected static $defaultName = 'app:history:subscription';
    protected static $defaultDescription = 'Subtracts subscription from current month bonus, if it is posible';

    /** @var BonusService  */
    private $bonusService;

    /** @var EntityManagerInterface  */
    private $em;

    /**
     * HistorySubscriptionCommand constructor.
     * @param BonusService $bonusService
     * @param EntityManagerInterface $em
     */
    public function __construct(BonusService $bonusService, EntityManagerInterface $em)
    {
        parent::__construct();
        $this->bonusService = $bonusService;
        $this->em = $em;
    }


    protected function configure()
    {
        $this
            ->setDescription(self::$defaultDescription)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Writing to database');

        $subscription = $this->em->getRepository(Subscription::class)->findOneBy(['name' => 'Abonament']);
        $users = $this->em->getRepository(User::class)->findAll();

        foreach ($users as $user) {
            $this->bonusService->updateSubscriptionHistoryBonus($user, $subscription->getValue());
        }

        $io->success('Update succeded!');

        return 0;
    }
}
