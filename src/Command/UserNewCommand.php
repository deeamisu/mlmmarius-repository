<?php

namespace App\Command;

use App\Entity\Role;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use OldSound\RabbitMqBundle\RabbitMq\ProducerInterface;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserNewCommand extends Command
{
    protected static $defaultName = 'app:user:new';

    private $em;
    private $passwordEncoder;

    public function __construct(EntityManagerInterface $em,  UserPasswordEncoderInterface $passwordEncoder)
    {
        parent::__construct();
        $this->em = $em;
        $this->passwordEncoder = $passwordEncoder;
    }

    protected function configure()
    {
        $this
            ->setDescription('Add a new user.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        do {
            $username = $io->ask('Enter username (required)');
        } while( empty($username) );

        do {
            $password = $io->ask('Enter user password (required)');
        } while( empty($password) );

        $roleList = explode(',' ,$io->ask('Enter roles comma separated'));

        $roles = $this->em->getRepository(Role::class)->findBy(['name'=>$roleList]);

        $user = new User();

        $password = $this->passwordEncoder->encodePassword($user, $password);

        $user->setUsername($username)
            ->setPassword($password)
        ;
        foreach ($roles as $role){
            $user->addUserRole($role);
        }

        $this->em->persist($user);
        $this->em->flush();

        $io->success(sprintf('User %s saved.', $username));

        return 0;
    }
}