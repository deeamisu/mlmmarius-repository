<?php

namespace App\Command;

use App\Service\BonusService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class MonthlyBonusCommand extends Command
{
    protected static $defaultName = 'app:monthly-bonus';

    protected $bonusService;

    /**
     * MonthlyBonusCommand constructor.
     * @param $bonusService BonusService
     */
    public function __construct(BonusService $bonusService)
    {
        parent::__construct();
        $this->bonusService = $bonusService;
    }


    protected function configure()
    {
        $this
            ->setDescription('Write monthly bonus to db')
            //->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            //->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        /*
        $arg1 = $input->getArgument('arg1');

        if ($arg1) {
            $io->note(sprintf('You passed an argument: %s', $arg1));
        }

        if ($input->getOption('option1')) {
            // ...
        }
        */
        $io->title('Writing to database');
        $this->bonusService->setBonusHistory();
        $io->success('Update succeded !');

        return 0;
    }
}
