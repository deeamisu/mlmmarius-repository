<?php
/**
 * Created by PhpStorm.
 * User: Misu
 * Date: 10/31/2020
 * Time: 2:17 AM
 */

namespace App\Interfaces;


use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Security;

interface UsefullServices
{
    /**
     * @return Security
     */
    public function getSecurity();

    /**
     * @return UserPasswordEncoderInterface
     */
    public function getPasswordEncoder();

    /**
     * @return EntityManagerInterface
     */
    public function getEntityManager();

    /**
     * @return UrlGeneratorInterface
     */
    public function getRouter();

    /**
     * @return ParameterBagInterface
     */
    public function getParameterBag();

    /**
     * @return MailerInterface
     */
    public function getMailer();

}