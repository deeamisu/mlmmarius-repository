<?php
/**
 * Created by PhpStorm.
 * User: Misu
 * Date: 10/31/2020
 * Time: 2:18 AM
 */

namespace App\Interfaces;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Security;

class ServicesToUse implements UsefullServices
{
    public $security;

    public $passwordEncoder;

    public $entityManager;

    public $router;

    public $parameterBag;

    public $mailer;

    /**
     * ServicesToUse constructor.
     * @param $security Security
     * @param $passwordEncoder UserPasswordEncoderInterface
     * @param $entityManager EntityManagerInterface
     * @param $urlGenerator UrlGeneratorInterface
     * @param $parameterBag ParameterBagInterface
     * @param $mailer MailerInterface
     */
    public function __construct(
        Security $security,
        UserPasswordEncoderInterface $passwordEncoder,
        EntityManagerInterface $entityManager,
        UrlGeneratorInterface $urlGenerator,
        ParameterBagInterface $parameterBag,
        MailerInterface $mailer
    )
    {
        $this->security = $security;
        $this->passwordEncoder = $passwordEncoder;
        $this->entityManager = $entityManager;
        $this->router = $urlGenerator;
        $this->parameterBag = $parameterBag;
        $this->mailer = $mailer;
    }

    /**
     * @return Security
     */
    public function getSecurity()
    {
        return $this->security;
    }

    /**
     * @return UserPasswordEncoderInterface
     */
    public function getPasswordEncoder()
    {
        return $this->passwordEncoder;
    }

    /**
     * @return EntityManagerInterface
     */
    public function getEntityManager()
    {
        return $this->entityManager;
    }

    /**
     * @return UrlGeneratorInterface
     */
    public function getRouter()
    {
        return $this->router;
    }

    /**
     * @return ParameterBagInterface
     */
    public function getParameterBag()
    {
        return $this->parameterBag;
    }

    /**
     * @return MailerInterface
     */
    public function getMailer()
    {
        return $this->mailer;
    }



}