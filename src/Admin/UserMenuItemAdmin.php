<?php

declare(strict_types=1);

namespace App\Admin;

use App\Entity\UserMenuItem;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

final class UserMenuItemAdmin extends AbstractAdmin
{

    protected function configureDatagridFilters(DatagridMapper $filter): void
    {
        $filter
            ->add('id')
            ->add('name')
            ->add('icon')
            ->add('route')
            ;
    }

    protected function configureListFields(ListMapper $list): void
    {
        $list
            ->add('id')
            ->add('name')
            ->add('icon')
            ->add('atributes')
            ->add('route')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $form): void
    {
        $form
            //->add('id')
            ->add('name')
            ->add('icon')
            ->add('route')
            //->add('atributes')
            ;
    }

    protected function configureShowFields(ShowMapper $show): void
    {
        $show
            ->add('id')
            ->add('name')
            ->add('icon')
            ->add('atributes')
            ->add('route')
            ;
    }

    public function toString($object)
    {
        return $object instanceof UserMenuItem
            ? $object->getName()
            : 'Menu Item'; // shown in the breadcrumb on the create view
    }
}
