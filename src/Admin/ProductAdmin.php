<?php

declare(strict_types=1);

namespace App\Admin;

use App\Entity\Product;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\FileType;

final class ProductAdmin extends AbstractAdmin
{
    /** @var string $productImageFolder */
    protected $rootDirectory;

    /**
     * @inheritDoc
     */
    public function __construct($code, $class, $baseControllerName, $rootDirectory)
    {
        parent::__construct($code, $class, $baseControllerName);
        $this->rootDirectory = $rootDirectory;
    }


    protected function configureDatagridFilters(DatagridMapper $filter): void
    {
        $filter
            ->add('id')
            ->add('name')
            ->add('description')
            ->add('value')
            ;
    }

    protected function configureListFields(ListMapper $list): void
    {
        $list
            ->add('id')
            ->add('name')
            ->add('description')
            ->add('value')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $form): void
    {
        $form
            ->add('name')
            ->add('description')
            ->add('value')
            ->add('file', FileType::class, [
                'required' => false
            ])
            ;
    }

    protected function configureShowFields(ShowMapper $show): void
    {
        $show
            ->add('id')
            ->add('name')
            ->add('description')
            ->add('value')
            ;
    }

    public function prePersist($object)
    {
        $this->manageFileUpload($object);
    }

    public function preUpdate($object)
    {
        $this->manageFileUpload($object);
    }

    private function manageFileUpload($product)
    {
        if ($product->getFile()) {
            $product->refreshUpdated($this->rootDirectory);
        }
    }

    /**
     * @param $object
     * @return string
     */
    public function toString($object): string
    {
        return $object instanceof Product
            ? $object->getName()
            : 'Produs'; // shown in the breadcrumb on the create view
    }
}
