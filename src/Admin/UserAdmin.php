<?php

declare(strict_types=1);

namespace App\Admin;

use App\Entity\User;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

final class UserAdmin extends AbstractAdmin
{

    protected function configureDatagridFilters(DatagridMapper $filter): void
    {
        $filter
            ->add('id')
            ->add('username')
            ->add('password')
            ->add('name')
            ->add('surname')
            ->add('mail')
            ->add('phone')
            ->add('adress')
            ->add('city')
            ->add('county')
            ->add('status')
            ->add('sponsor')
            ->add('clients')
            ;
    }

    protected function configureListFields(ListMapper $list): void
    {
        $list
            ->add('id')
            ->add('username')
            //->add('password')
            ->add('name')
            ->add('surname')
            ->add('mail')
            ->add('phone')
            ->add('adress')
            ->add('city')
            ->add('county')
            ->add('status')
            ->add('sponsor')
            ->add('clients')
            ->add('_action', null, [
                'actions' => [
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $form): void
    {
        $form
            ->add('username')
            ->add('password')
            ->add('name')
            ->add('surname')
            ->add('mail')
            ->add('phone')
            ->add('adress')
            ->add('city')
            ->add('county')
            ->add('status')
            ->add('sponsor')
            ->add('clients')
            ->add('userRoles')
            ->add('actions')
            ;
    }

    public function toString($object)
    {
        return $object instanceof User
            ? $object->getUsername()
            : 'User'; // shown in the breadcrumb on the create view
    }
}
