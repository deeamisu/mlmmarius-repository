<?php

namespace App\Form;

use App\Entity\User;
use App\Service\GeneralService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;

class ModalRegistrationUserType extends AbstractType
{
    private $generalService;

    /**
     * ModalRegistrationUserType constructor.
     * @param $generalService GeneralService
     */
    public function __construct(GeneralService $generalService)
    {
        $this->generalService = $generalService;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //->add('username')
            //->add('password')
            //->add('confirmationToken')
            //->add('passwordRequestedAt')
            //->add('name')
            //->add('surname')
            //->add('mail')
            //->add('phone')
            //->add('city')
            //->add('adress')
            //->add('county')
            //->add('userRoles')
            //->add('actions')
            //->add('sponsor')
            //->add('status')
            ->add('mail', EmailType::class, [
                'label' => 'Email',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Introduceti adresa de email',
                    'style' => 'margin-bottom:10px',
                    'pattern' => "[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$"
                ],
                'constraints' => [
                    new Email([
                        'mode' => 'loose',
                        'message' => 'Email incorect !',
                        'groups' => 'registration'
                    ])
                ]
            ])
            ->add('trimite', SubmitType::class,[
                'attr' => [
                    'class' => 'btn btn-success waves-effect waves-light',
                    'style' => 'float:right; margin:5px 0px 20px 5px'
                ]
            ])
            ->add('link', TextareaType::class, [
                'mapped' => false,
                'label' => 'Link pentru inscriere',
                'label_attr' => [
                    'style' => 'margin-top:20px',
                ],
                'data' => $this->generalService->getRegistrationLink(),
                'attr' => [
                    'class' => 'form-control',
                    'style' => 'margin-bottom:10px',
                ]
            ])
            ->add('copiaza link', ButtonType::class, [
                'attr' => [
                    'class' => 'btn btn-success waves-effect waves-light',
                    'style' => 'float:right; margin:5px 0px 5px 5px',
                    'onclick' => 'copyLink()',
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'validation_groups' => ['registration'],
        ]);
    }
}
