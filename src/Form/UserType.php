<?php

namespace App\Form;

use App\Entity\County;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //->add('username')
            //->add('password')
            //->add('confirmationToken')
            //->add('passwordRequestedAt')
            //->add('name')
            //->add('surname')
            //->add('mail')
            //->add('phone')
            //->add('city')
            //->add('userRoles')
            //->add('actions')
            //->add('consumer')
            //->add('sponsor')
            ->add('name', TextType::class, [
                'label' => 'Nume',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Introduceti numele',
                    'style' => 'margin-bottom:10px'
                ]
            ])
            ->add('surname', TextType::class, [
                'label' => 'Prenume',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Introduceti prenumele',
                    'style' => 'margin-bottom:10px'
                ]
            ])
            ->add('mail', EmailType::class, [
                'label' => 'Email',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Introduceti adresa de email',
                    'style' => 'margin-bottom:10px',
                    'pattern' => "[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$"
                ],
                'constraints' => [
                    new Email([
                        'mode' => 'loose',
                        'message' => 'Email incorect !',
                        'groups' => 'registration'
                    ])
                ]
            ])
            ->add('phone', TextType::class, [
                'label' => 'Mobil',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Introduceti numarul de telefon',
                    'style' => 'margin-bottom:10px',
                    'pattern' => "(07)\d{8}$"
                ]
            ])
            ->add('adress', TextType::class, [
                'label' => 'Adresa (ex: Strada Petru Rares Nr 4 Bl P28 Sc B Ap 17)',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Introduceti adresa(Str /Nr /Bl /Sc /Ap )',
                    'style' => 'margin-bottom:10px'
                ]
            ])
            ->add('city', TextType::class, [
                'label' => 'Oras',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Introduceti localitatea',
                    'style' => 'margin-bottom:10px'
                ]
            ])
            ->add('county', EntityType::class, [
                'label' => 'Judet',
                'class' => County::class,
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Introduceti judetul',
                    'style' => 'margin-bottom:10px'
                ]
            ])
            ->add('firma', ButtonType::class, [
                'attr' => [
                    'class' => 'btn btn-success waves-effect waves-light',
                    'style' => 'float:right; margin:5px 0px 5px 5px'
                ]
            ])
            ->add('company', TextType::class, [
                'label' => 'Denumire firma',
                'label_attr' => [
                    'style' => 'margin-top:40px'
                ],
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Introduceti numele firmei',
                    'style' => 'margin-bottom:10px'
                ],
                'required' => false
            ])
            ->add('cui', TextType::class, [
                'label' => 'CUI',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Introduceti CUI-ul firmei',
                    'style' => 'margin-bottom:10px'
                ],
                'required' => false
            ])
            ->add('companyAdress', TextType::class, [
                'label' => 'Adresa firma',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Introduceti adresa firmei',
                    'style' => 'margin-bottom:10px'
                ],
                'required' => false
            ])
            ->add('acordGDPR', CheckboxType::class, [
                'mapped' => false,
                'label' => 'Acord GDPR. Sunt de acord cu prelucrarea datelor personale. Înțeleg că nu vor fi folosite pentru alte scopuri, decât cele de înregistrare în Echipa Vultur și că nu vor fi transmise către terțe părți.',
                'label_attr' => [
                    'style' => 'float:right; margin-left:25px;'
                ],
                'attr' => [
                    'style' => 'position:absolute; float:left; margin-top:50px'
                ]
            ])
            ->add('terms', CheckboxType::class, [
                'mapped' => false,
                'attr' => [
                    'style' => 'position:absolute; float:left; margin-top:3px '
                ],
                'label' => 'Sunt de acord cu termenii si conditiile de adeziune',
                'label_attr' => [
                    'style' => 'float:right; margin-left:25px; margin-right:35px'
                ],
            ])
            ->add('salvare', SubmitType::class,[
                'attr' => [
                    'class' => 'btn btn-success waves-effect waves-light',
                    'style' => 'float:right; margin:5px 0px 5px 5px'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'validation_groups' => ['registration'],
        ]);
    }
}
