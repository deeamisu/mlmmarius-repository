<?php


namespace App\Service;


use App\Entity\Action;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ControllerResolverInterface;
use Symfony\Component\HttpKernel\Controller\TraceableControllerResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class SecurityVoter extends Voter
{
    /** @var TraceableControllerResolver */
    private $controllerResolver;

    /** @var EntityManagerInterface */
    private $em;

    /** @var User */
    private $user;

    private $userActions = [];

    private $userRoles = [];
    /**
     * SecurityVoter constructor.
     */
    public function __construct(ControllerResolverInterface $controllerResolver, EntityManagerInterface $em, TokenStorageInterface $tokenStorage)
    {
        $this->controllerResolver = $controllerResolver;
        $this->em = $em;
        if ($tokenStorage->getToken() && $tokenStorage->getToken()->getUser() instanceof User){
            $this->user = $tokenStorage->getToken()->getUser();
            $this->userActions = $this->getAllActions($this->user);
            $this->userRoles = $this->user->getRoles();
        } else {
            $this->user = null;
        }
    }


    /**
     * @inheritDoc
     */
    protected function supports($attribute, $subject)
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        if ($subject instanceof Request){
            $controller = $this->controllerResolver->getController($subject);
            if (!is_array($controller)){
                return true;
            }
            $controllerNamespaces = explode('\\', get_class($controller[0]));
            $controllerName = str_replace('Controller', '', array_pop($controllerNamespaces));
            $actionName = $controller[1];

            $actions = $this->em->getRepository(Action::class)->findBy([
                'name'=>[
                    $controllerName.':*', //if is allowed for all actions of the controller
                    $controllerName.':'.$actionName,
                ]
            ]);
            if (empty($actions)){
                return true; //if no action is defined then all actions ar permited for logged in users
            }


            return $this->hasAction($controllerName.':'.$actionName);
        } else {
            return $this->hasAction($attribute);
        }
    }

    public function getAllActions(User $user)
    {
        $userActions = [];

        foreach ($user->getActions() as $action){
            $userActions[$action->getName()]=$action->getName();
        }
        foreach ($user->getUserRoles() as $role) {
            foreach ($role->getActions() as $action) {
                $userActions[$action->getName()]=$action->getName();
            }
        }

        return $userActions;
    }

    public function hasRole($name)
    {
        return in_array($name, $this->userRoles);
    }

    public function hasAction($name)
    {
        $actionParts = explode(':', $name);
        $controller = $actionParts[0];
        return in_array($controller.':*', $this->userActions) || in_array($name, $this->userActions);
    }

}