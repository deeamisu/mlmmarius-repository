<?php

namespace App\Service;

use App\Entity\CartItem;
use Doctrine\ORM\EntityManagerInterface;

class OrderService
{
    /** @var EntityManagerInterface  */
    private $em;

    /**
     * OrderService constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param CartItem $cartItem
     */
    public function removeCartItem(CartItem $cartItem)
    {
        if ($cartItem->getQuantity() < 2) {
            $this->em->remove($cartItem);
        }

        $cartItem->setQuantity($cartItem->getQuantity() - 1);
        $this->em->flush();
    }
}