<?php
/**
 * Created by PhpStorm.
 * User: Misu
 * Date: 11/25/2020
 * Time: 4:05 AM
 */

namespace App\Service;

use App\Entity\Bonus;
use App\Entity\HistoryBonus;
use App\Entity\Status;
use App\Entity\Subscription;
use App\Entity\User;
use App\Traits\TimeZone;
use DateTime;

class BonusService extends GeneralService
{
    use TimeZone;

    /**
     * @param $user User
     * @param $circles array
     */
    private function getBonusCircles($user, $circles, $index)
    {
        $count = $index + 1;
        if ($count > 3) {
            return $circles;
        } else {
            $consumers = $user->getClients();
            foreach ($consumers as $consumer) {
                if ($consumer->getStatus()->getName() == 'Activ') {
                    $circles[$index][] = $consumer;
                } else if ($consumer->getStatus()->getName() == 'Inactiv') {
                    $circles[$index][] = null;
                }
                $circles = $this->getBonusCircles($consumer, $circles, $count);
            }
            return $circles;
        }
    }

    /**
     * @param $user User
     * @return array
     */
    public function setBonusCircles($user)
    {
        $circles = [];
        $index = 0;
        $circles = $this->getBonusCircles($user, $circles, $index);
        if (count($circles) < 3) {
            for ($i = count($circles); $i < 3; $i++) {
                $circles[]= [];
            }
        }
        return $circles;
    }

    /**
     * @param $circles array
     * @param $user User
     * @return int
     */
    public function getMonthlyBonus($circles,$user)
    {
        $entityManager = $this->usefullServices->getEntityManager();
        $subscription = $entityManager->getRepository(Subscription::class)->findOneBy([
            'name' => 'Abonament'
        ]);
        $monthlyBonus = 0;
        if ($subscription && count($circles) > 0) {
            if ($user->getStatus() == 'Activ') {
                $bonus = $entityManager->getRepository(Bonus::class)->findOneBy([
                    'keyValue' => 0
                ]);
                $monthlyBonus += count(array_filter($circles[0], function ($val){return $val !== null;})) * ($subscription->getValue() / (100 / $bonus->getProcentValue()));
                if (count($circles[0]) > 2) {
                    for ($index = 1; $index < 3; $index++) {
                        $bonus = $entityManager->getRepository(Bonus::class)->findOneBy([
                            'keyValue' => $index
                        ]);
                        $monthlyBonus += count(array_filter($circles[$index], function ($val){return $val !== null;})) * ($subscription->getValue() / (100 / $bonus->getProcentValue()));
                    }
                }
            }
        }
        return $monthlyBonus;
    }

    /**
     * @param $circles array
     * @param $user User
     * @return int
     */
    public function getTotalBonus($user)
    {
        $entityManager = $this->usefullServices->getEntityManager();
        $bonuses = $entityManager->getRepository(HistoryBonus::class)->findBy([
            'user' => $user
        ]);

        $totalBonus = 0;

        foreach ($bonuses as $bonus){
            $totalBonus+=$bonus->getValue();
        }

        return $totalBonus;
    }

    public function setBonusHistory()
    {
        $em = $this->usefullServices->getEntityManager();
        $users = $em->getRepository(User::class)->findAll();
        foreach ($users as $user) {
            $historyBonus = new HistoryBonus();
            $historyBonus
                ->setUser($user)
                ->setValue($this->getMonthlyBonus($this->setBonusCircles($user), $user))
                ->setCreatedAt($this->setBonusHistoryDate())
                ->setName('Bonus')
            ;
            $em->persist($historyBonus);
            $em->flush();
        }
    }

    private function setBonusHistoryDate()
    {
        $date = new DateTime();
        return $date->sub(new \DateInterval('P15D'));
    }

    /**
     * @param User $user
     * @param int $subscription
     */
    public function updateSubscriptionHistoryBonus(User $user, int $subscription)
    {
        $date = new DateTime('now');

        $em = $this->usefullServices->getEntityManager();
        if ($this->getSold($user) > $subscription ) {
            $historyEntry = new HistoryBonus();
            $historyEntry
                ->setName('Abonament '. $this->getMonthYear($date->format('Y-m-d')))
                ->setValue(-$subscription)
                ->setCreatedAt($date)
                ->setUser($user)
            ;
            $em->persist($historyEntry);
        } else {
            $status = $em->getRepository(Status::class)->findOneBy(['name' => 'Inactiv']);
            $user->setStatus($status);
        }
        $em->flush();
    }

    /**
     * @param User $user
     * @return int
     */
    public function getSold(User $user): int
    {
        $total = 0;

        foreach ($user->getHistoryBonuses() as $historyBonus) {
            $total += $historyBonus->getValue();
        }

        return $total;
    }
}