<?php
/**
 * Created by PhpStorm.
 * User: Misu
 * Date: 10/25/2020
 * Time: 11:05 PM
 */

namespace App\Service;


use App\Entity\County;
use App\Entity\Role;
use App\Entity\Status;
use App\Entity\User;
use App\Interfaces\UsefullServices;
use Endroid\QrCode\Factory\QrCodeFactory;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;
use League\Csv\Reader;
use Symfony\Component\Routing\RouterInterface;

class GeneralService
{

    protected $usefullServices;
    
    protected $router;


    /**
     * GeneralService constructor.
     * @param $usefullServices UsefullServices
     */
    public function __construct(UsefullServices $usefullServices, RouterInterface $router)
    {
        $this->usefullServices = $usefullServices;
        $this->router = $router;
    }

    /**
     * @param $consumer User
     * @param $index integer
     * @param $circle integer[]
     */
    private function getCircles($consumer, $index, $circle)
    {
        if ($index < 10) {
            $circle[$index]['count'] = $circle[$index]['count'] + count($consumer->getClients());

            foreach ($consumer->getClients() as $client) {
                $circle[$index]['consumer'][]= $client;
                if (count($client->getClients()) > 0) {
                    $circle = $this->getCircles($client, $index+1, $circle );
                }
            }
        }
        return $circle;
    }

    public function setCircle()
    {
        /** @var User $consumer */
        $consumer = $this->usefullServices->getSecurity()->getUser();
        $init = [];
        for ($i = 0; $i < 10; $i++) {
            $init[$i]['count'] = 0;
            $init[$i]['consumer'] = null;
        }
        $circle = $this->getCircles($consumer, 0, $init);
        return $circle;
    }

    /**
     * @param $consumer User
     * @param $x
     * @param $table
     * @return mixed
     */
    public function getNetworkTable ($consumer, $x, $table, $circle)
    {
        foreach ($consumer->getClients() as $client) {
            //if ( !(is_null($client->getStatus()))  && ($client->getStatus()->getName() == 'Activ') ) {
                $y = $this->validatePos($circle,$client);
                $table[$x][$y] = $client;
                $x++;
                $table = $this->getNetworkTable($client, $x, $table, $circle);
                $x = count($table) ;
            //}
        }
        return $table;
    }

    public function setNetworkTable($circle)
    {
        $consumer = $this->usefullServices->getSecurity()->getUser();
        $table = $this->getNetworkTable($consumer, 0, [], $circle);
        return $table;
    }

    private function validatePos($circle, $consumer)
    {
        foreach ($circle as $key => $item) {
            foreach ($item['consumer'] as $value)
                if ($value->getId() == $consumer->getId()) {
                    return $key;
                }
        }
        return null;
    }

    /**
     * @param $user User
     */
    public function encodePassword($user, $password)
    {
        $passwordEncoder = $this->usefullServices->getPasswordEncoder();
        return $passwordEncoder->encodePassword($user,$password);
    }

    public function getStars()
    {
        /**
         * @var $security Security
         * @var $user User
         */
        $security = $this->usefullServices->getSecurity();
        $user = $security->getUser();
        $firstCircle = count($user->getClients());
        if ($firstCircle > 2) {
            $count = $this->countClients($user, 0);
            if ($count > 149) {
                return 5;
            }
            if ($count > 79) {
                return 4;
            }
            if ($count > 39) {
                return 3;
            }
            if ($count > 19) {
                return 2;
            }
            if ($count > 9) {
                return 1;
            }
        }
        return 0;
    }

    /**
     * @param $user User
     * @param $count
     */
    public function countClients($user, $count)
    {
        $clients = $user->getClients();
        $count = $count + count($clients);
        if (count($clients) > 0) {
            foreach ($clients as $client) {
                $count = $this->countClients($client,$count);
            }
        }
        return $count;
    }

    public function getMonth()
    {
        date_default_timezone_set('Europe/Bucharest');
        setlocale(LC_ALL, 'ro_RO.utf8');
        $date = new \DateTime('@'.strtotime('now'));
        $currentMonth = strftime('%B', strtotime($date->format('d-m-Y')));
        return ucfirst($currentMonth);
    }

    public function getRegistrationLink()
    {
        /**
         * @var $security Security
         * @var $urlGenerator UrlGeneratorInterface
         * @var $user User
         */
        $security = $this->usefullServices->getSecurity();
        $user = $security->getUser();
        $urlGenerator = $this->usefullServices->getRouter();
        return  $urlGenerator->generate('registration_form', [
           'id' => $user->getId(),
        ],
            UrlGeneratorInterface::ABSOLUTE_URL
        );
    }

    public function getQrCodeRegistrationLink()
    {
        $parameterBag = $this->usefullServices->getParameterBag();
        $url = $this->getRegistrationLink();
        $qrCodeFactory = new QrCodeFactory();
        $qrcode = $qrCodeFactory->create($url,$parameterBag->get('endroid_qr_code'));
        return $qrcode;
    }

    /**
     * @param $consumer User
     */
    public function sendConfirmations($consumer, $password)
    {
        $mailer = $this->usefullServices->getMailer();

        // Send mail to sponsor
        $email = (new TemplatedEmail())
            ->from('noreply@echipavultur.ro')
            ->to(new Address($consumer->getMail()))
            ->subject('Bine ai venit in Echipa Vultur')

            // path of the Twig template to render
            ->htmlTemplate('email/registration.html.twig')

            // pass variables (name => value) to the template
            ->context([
                'consumer' => $consumer,
                'password' => $password,
            ])
        ;
        $mailer->send($email);

        // Send mail to sponsor
        $email = (new TemplatedEmail())
            ->from('noreply@echipavultur.ro')
            ->to(new Address($consumer->getSponsor()->getMail()))
            ->subject('Inscriere noua in reteaua ta')

            // path of the Twig template to render
            ->htmlTemplate('email/confirmation.html.twig')

            // pass variables (name => value) to the template
            ->context([
                'consumer' => $consumer,
            ])
        ;
        $mailer->send($email);

        //Send mails to admins

        $entityManager = $this->usefullServices->getEntityManager();
        $role = $entityManager->getRepository(Role::class)->findOneBy(['name' => 'ROLE_ADMIN']);
        $admins = $role->getUsers();
        foreach ($admins as $admin) {
            $email = (new TemplatedEmail())
                ->from('noreply@echipavultur.ro')
                ->to(new Address($admin->getMail()))
                ->subject('Inscriere noua in platforma')

                // path of the Twig template to render
                ->htmlTemplate('email/admin_confirmation.html.twig')

                // pass variables (name => value) to the template
                ->context([
                    'consumer' => $consumer,
                ])
            ;
            $mailer->send($email);
        }
    }

    public function importCsv()
    {

        $csvPath = $this->usefullServices->getParameterBag()->get('project_dir').'/public/csv/model.csv';
        $entityManager = $this->usefullServices->getEntityManager();

        //load the CSV document from a file path
        $csv = Reader::createFromPath($csvPath, 'r');
        $csv->setHeaderOffset(0);

        $header = $csv->getHeader(); //returns the CSV header record
        $records = $csv->getRecords(); //returns all the CSV records as an Iterator object

        foreach ($records as $record) {
            $consumer = new User();
            $idSponsor = $this->transformCsvId($record['ID Sponsor']);
            $sponsor = $entityManager->find(User::class, $idSponsor);
            $county = $entityManager->getRepository(County::class)->findOneBy(['name' => $record['Judet']]);
            $role = $entityManager->getRepository(Role::class)->findOneBy(['name' => 'ROLE_USER']);
            $status = $entityManager->getRepository(Status::class)->findOneBy(['name' => 'Activ']);
            $fullName = $this->transformName($record['Denumire client']);
            $password = rand(100000,999999);
            $consumer->setName($fullName['name'])
                ->setSurname($fullName['surname'])
                ->setMail($record['Email'])
                ->setUsername($record['Email'])
                ->setAdress($record['Adresa'])
                ->setCity($record['Localitate'])
                ->setCounty($county)
                ->setStatus($status)
                ->setPhone('0' . $record['Telefon'])
                ->setPassword($this->encodePassword($consumer, $password))
                ->setSponsor($sponsor)
            ;
            $consumer->addUserRole($role);
            $entityManager->persist($consumer);
            $entityManager->flush();
            
            $mailer = $this->usefullServices->getMailer();
            $context = $this->router->getContext();
            $context->setHost('app.echipavultur.ro');
            $context->setScheme('http');
            // Send mail to sponsor
            $email = (new TemplatedEmail())
                ->from('noreply@echipavultur.ro')
                ->to(new Address($consumer->getMail()))
                ->subject('Bine ai venit in Echipa Vultur')
    
                // path of the Twig template to render
                ->htmlTemplate('email/registration.html.twig')
    
                // pass variables (name => value) to the template
                ->context([
                    'consumer' => $consumer,
                    'password' => $password,
                ])
            ;
            $mailer->send($email);
            
            $entityManager->clear();
            sleep(1);
        }
    }

    private function transformCsvId($text)
    {
        $transformedText = str_replace('RO', '', $text);
        return intval($transformedText);
    }

    private function transformName($text)
    {
        $fullName = [];
        $nameLength = strpos($text, ' ', 1);
        $fullName['name'] = substr($text,0,$nameLength);
        $fullName['surname'] = substr($text, $nameLength + 1);
        return $fullName;
    }
}