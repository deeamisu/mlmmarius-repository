<?php

namespace App\Service;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ProfilePhotoUploadService
{
    /** @var ValidatorInterface  */
    protected $validator;

    /** @var EntityManagerInterface  */
    protected $em;

    /** @var ParameterBagInterface */
    protected $parameterBag;

    /**
     * ProfilePhotoUploadService constructor.
     * @param ValidatorInterface $validator
     * @param EntityManagerInterface $em
     * @param ParameterBagInterface $parameterBag
     */
    public function __construct(
        ValidatorInterface $validator,
        EntityManagerInterface $em,
        ParameterBagInterface $parameterBag
    )
    {
        $this->validator = $validator;
        $this->em = $em;
        $this->parameterBag = $parameterBag;
    }

    /**
     * @param User $user
     * @param UploadedFile $file
     * @param Request $request
     * @return User
     */
    public function uploadPhoto(User $user, UploadedFile $file, Request $request): ?User
    {
        if (!$this->validateUploadedPhoto($file, $request)) return $user;
        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        // this is needed to safely include the file name as part of the URL
        $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
        $newFilename = $safeFilename.'-'.uniqid().'.'.$file->guessExtension();

        try {
            $file->move(
                $this->parameterBag->get('photos'),
                $newFilename
            );
        } catch (FileException $e) {
            // ... handle exception if something happens during file upload
        }

        if ($this->hasUserPhoto($user)) {
            $fileSystem = new Filesystem();
            $previousPhoto = $this->parameterBag->get('photos') . '/' . $user->getPhoto();
            $fileSystem->remove($previousPhoto);
        }
        $user->setPhoto($newFilename);

        return $user;
    }

    /**
     * @param UploadedFile $uploadedFile
     * @param Request $request
     * @return bool
     */
    private function validateUploadedPhoto(UploadedFile $uploadedFile, Request $request): bool
    {
        $constraints = new File(
            [
            'mimeTypesMessage' => 'Fisierul incarcat nu este intr-un format corespunzator',
            'mimeTypes' => [
                'image/jpeg',
                'image/png',
                'image/jpg'
            ]
            ]
        );
        $errors = $this->validator->validate($uploadedFile,$constraints);
//        if (count($errors) > 0) {
//            $request->getSession()->getFlashBag()->add('notice', 'Fisierul incarcat nu este intr-un format corespunzator');
//        }
        return count($errors) == 0;
    }

    /**
     * @param User $user
     * @return bool
     */
    private function hasUserPhoto(User $user): bool
    {
        return $user->getPhoto() != null;
    }
}