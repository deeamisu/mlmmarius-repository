<?php
/**
 * Created by PhpStorm.
 * User: Misu
 * Date: 11/11/2020
 * Time: 3:13 AM
 */

namespace App\EventSubscriber;


use App\Event\RegistrationConfirmationEvent;
use App\Service\GeneralService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class RegistrationConfirmationSubscriber implements EventSubscriberInterface
{
    private $generalServices;

    /**
     * AcountDetailsSubscriber constructor.
     * @param $generalServices GeneralService
     */
    public function __construct(GeneralService $generalServices)
    {
        $this->generalServices = $generalServices;
    }

    public static function getSubscribedEvents()
    {
        return [
            RegistrationConfirmationEvent::NAME => 'confirmation',
        ];
    }

    public function confirmation(RegistrationConfirmationEvent $event)
    {
        $this->generalServices->sendConfirmations($event->getConsumer(), $event->getPassword());
    }
}