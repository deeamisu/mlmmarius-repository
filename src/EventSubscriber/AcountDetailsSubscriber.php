<?php

namespace App\EventSubscriber;

use App\Entity\User;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\File;

class AcountDetailsSubscriber implements EventSubscriberInterface
{
    public function preSetData(FormEvent $event)
    {
        /**
         * @var $user User
         */
        $user = $event->getData();
        $form = $event->getForm();

        $form
            ->add('mail', EmailType::class, [
                'data' => $user->getMail(),
                'required' => false,
                'empty_data' => '',
                'label' => 'Email',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Introduceti adresa de email',
                    'style' => 'margin-bottom:10px',
                    'pattern' => "[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$"
                ],
                'constraints' => [
                    new Email([
                        'mode' => 'loose',
                        'message' => 'Email incorect !',
                        'groups' => 'registration'
                    ])
                ]
            ])
            ->add('phone', TextType::class, [
                'label' => 'Mobil',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Introduceti numarul de telefon',
                    'style' => 'margin-bottom:10px',
                    'pattern' => "(07)\d{8}$"
                ],
                'required' => false,
                'empty_data' => '',
                'data' => $user->getPhone()
            ])
            ->add('password', PasswordType::class ,[
                'label' => 'Parola noua',
                'attr' => [
                    'class' => 'form-control',
                    'style' => 'margin-bottom:10px',
                    'placeholder' => 'Parola noua'
                ],
                'required' => false,
                'empty_data' => '',
            ])
            ->add('password2', PasswordType::class ,[
                'label' => 'Confirmare parola noua',
                'attr' => [
                    'class' => 'form-control',
                    'style' => 'margin-bottom:10px',
                    'placeholder' => 'Tastati noua parola din nou',
                ],
                'required' => false,
                'empty_data' => '',
                'mapped' => false,
            ])
            ->add('ph', FileType::class, [
                'mapped' => false,
                'required' => false,

                'constraints' => new File([
                    'mimeTypes' => [
                        'image/png',
                        'image/jp2',
                    ],
                    'mimeTypesMessage' => 'Incarcati o poza in format png, jpg sau jpeg',
                ]),
                'label' => 'Adauga o poza',
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('save', SubmitType::class,[
                'attr' => [
                    'class' => 'btn btn-success waves-effect waves-light',
                    'style' => 'float:right; margin:5px 0px 5px 5px'
                ]
            ])
            ;
    }

    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData',
        ];
    }
}
