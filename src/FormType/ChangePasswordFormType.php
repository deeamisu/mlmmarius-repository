<?php


namespace App\FormType;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;

class ChangePasswordFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //->add('password', PasswordType::class)
            ->add('plainPassword', RepeatedType::class, array(
                'type'              => PasswordType::class,
                'required'          => false,
                'first_options'     => array('label' => 'New password', "attr" => ["class" => "form-control"]),
                'second_options'    => array('label' => 'Confirm new password', "attr" => ["class" => "form-control"]),
                'invalid_message' => 'The password fields must match.',
            ))
        ;
    }

}