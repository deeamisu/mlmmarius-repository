<?php


namespace App\FormType;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Email;

class ForgottenUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("username", null, array(
                "label" => "Email",
                "attr" => array(
                    "class" => "form-control",
                    "id" => "basic-url",
                    "placeholder" => "Email address for your account"
                ),
                "constraints" => array(
                    new Email(array("message" => "Invalid Email"))
                )
            ));
    }
}