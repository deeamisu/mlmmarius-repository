<?php
namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="comenzi")
 */
class Comanda
{
   // use EntityTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $phone;

    /**
     * @Assert\Email(
     *     message = "Email-ul '{{ value }}' nu este valid."
     * )
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $email;

    /**
     * @ORM\Column(type="text")
     */
    protected $address;

    /**
     * Many Groups have Many Users.
     * @ORM\ManyToMany(targetEntity="Product", mappedBy="orders", cascade={"persist"})
     */
    protected $products;

    /**
     * @ORM\Column(type="decimal", precision=8, scale=2)
     */
    private $price;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $clientId;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $addressId;

//    /**
//     * @ORM\OneToMany(targetEntity="ComandaProdus", mappedBy="order", cascade={"persist"}, orphanRemoval=true)
//     */
//    protected $orderProducts;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $notified = false;

    /**
     * @ORM\Column(type="boolean", options={"default" : false})
     */
    protected $payCc = false;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $paymentStatus;

    /**
     * @ORM\OneToMany(targetEntity="PaymentLog", mappedBy="order", cascade={"persist"}, orphanRemoval=true)
     */
    protected $paymentLogs;

    public function __construct()
    {
        $this->orderProducts = new ArrayCollection();
        $this->products = new ArrayCollection();
        $this->paymentLogs = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address): void
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getOrderProducts()
    {
        return $this->orderProducts;
    }

    /**
     * @param mixed $orderProducts
     */
    public function setOrderProducts($orderProducts): void
    {
        $this->orderProducts = $orderProducts;
    }

    public function addOrderProduct($comandaProdus)
    {
        $comandaProdus->setOrder($this);
        $this->orderProducts->add($comandaProdus);
    }

    /**
     * @return mixed
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param mixed $products
     */
    public function setProducts($products): void
    {
        $this->products = $products;
    }

//    public function addProduct(Product $product)
//    {
//        $product->addOrder($this); // synchronously updating inverse side
//        $this->products[] = $product;
//    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price): void
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * @param mixed $clientId
     */
    public function setClientId($clientId): void
    {
        $this->clientId = $clientId;
    }

    /**
     * @return mixed
     */
    public function getAddressId()
    {
        return $this->addressId;
    }

    /**
     * @param mixed $addressId
     */
    public function setAddressId($addressId): void
    {
        $this->addressId = $addressId;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }


    /**
     * @return bool
     */
    public function getNotified()
    {
        return $this->notified;
    }


    /**
     * @param bool $notified
     */
    public function setNotified($notified)
    {
        $this->notified = $notified;
    }

    /**
     * @return mixed
     */
    public function getPayCc()
    {
        return $this->payCc;
    }

    /**
     * @param mixed $payCc
     */
    public function setPayCc($payCc): void
    {
        $this->payCc = $payCc;
    }

    /**
     * @return mixed
     */
    public function getPaymentStatus()
    {
        return $this->paymentStatus;
    }

    /**
     * @param mixed $paymentStatus
     */
    public function setPaymentStatus($paymentStatus): void
    {
        $this->paymentStatus = $paymentStatus;
    }

    /**
     * @return mixed
     */
    public function getPaymentLogs()
    {
        return $this->paymentLogs;
    }

    /**
     * @param mixed $paymentLogs
     */
    public function setPaymentLogs($paymentLogs): void
    {
        $this->paymentLogs = $paymentLogs;
    }

    public function addPaymentLog(PaymentLog $paymentLog)
    {
        $paymentLog->setOrder($this);
        $this->paymentLogs->add($paymentLog);
    }
}