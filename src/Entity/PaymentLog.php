<?php
namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 * @ORM\Table(name="payment_logs")
 */
class PaymentLog
{
    //use EntityTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Comanda", inversedBy="paymentLogs", cascade={"persist"})
     * @ORM\JoinColumn(name="comanda_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $order;

    /**
     * @ORM\Column(type="text")
     */
    protected $log;

    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param mixed $order
     */
    public function setOrder($order): void
    {
        $this->order = $order;
    }

    /**
     * @return mixed
     */
    public function getLog()
    {
        return $this->log;
    }

    /**
     * @param mixed $log
     */
    public function setLog($log): void
    {
        $this->log = $log;
    }

    public function __toString()
    {
        return $this->getLog()  . ' -------------- Data: ' . $this->createdAt->format('Y-m-d H:i:s') ;
        // TODO: Implement __toString() method.
    }
}