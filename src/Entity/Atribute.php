<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AtributeRepository")
 */
class Atribute
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\UserMenuItem", inversedBy="atributes")
     */
    private $userMenuItems;

    public function __construct()
    {
        $this->userMenuItems = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|UserMenuItem[]
     */
    public function getUserMenuItems(): Collection
    {
        return $this->userMenuItems;
    }

    public function addUserMenuItem(UserMenuItem $userMenuItem): self
    {
        if (!$this->userMenuItems->contains($userMenuItem)) {
            $this->userMenuItems[] = $userMenuItem;
        }

        return $this;
    }

    public function removeUserMenuItem(UserMenuItem $userMenuItem): self
    {
        if ($this->userMenuItems->contains($userMenuItem)) {
            $this->userMenuItems->removeElement($userMenuItem);
        }

        return $this;
    }

    function __toString()
    {
        return $this->getName();
    }


}
