<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BonusRepository")
 */
class Bonus
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $keyValue;

    /**
     * @ORM\Column(type="integer")
     */
    private $procentValue;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getKeyValue(): ?int
    {
        return $this->keyValue;
    }

    public function setKeyValue(int $keyValue): self
    {
        $this->keyValue = $keyValue;

        return $this;
    }

    public function getProcentValue(): ?int
    {
        return $this->procentValue;
    }

    public function setProcentValue(int $procentValue): self
    {
        $this->procentValue = $procentValue;

        return $this;
    }
}
