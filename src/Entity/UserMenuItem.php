<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserMenuItemRepository")
 */
class UserMenuItem
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $icon;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Atribute", mappedBy="userMenuItems")
     */
    private $atributes;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Route", inversedBy="userMenuItems")
     */
    private $route;

    public function __construct()
    {
        $this->atributes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    function __toString()
    {
        return $this->getName();
    }

    public function getIcon(): ?string
    {
        return $this->icon;
    }

    public function setIcon(?string $icon): self
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * @return Collection|Atribute[]
     */
    public function getAtributes(): Collection
    {
        return $this->atributes;
    }

    public function addAtribute(Atribute $atribute): self
    {
        if (!$this->atributes->contains($atribute)) {
            $this->atributes[] = $atribute;
            $atribute->addUserMenuItem($this);
        }

        return $this;
    }

    public function removeAtribute(Atribute $atribute): self
    {
        if ($this->atributes->contains($atribute)) {
            $this->atributes->removeElement($atribute);
            $atribute->removeUserMenuItem($this);
        }

        return $this;
    }

    public function getRoute(): ?Route
    {
        return $this->route;
    }

    public function setRoute(?Route $route): self
    {
        $this->route = $route;

        return $this;
    }


}
