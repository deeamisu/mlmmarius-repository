<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201023223050 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE atribute (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE atribute_user_menu_item (atribute_id INT NOT NULL, user_menu_item_id INT NOT NULL, INDEX IDX_F8DA03BFCAE21197 (atribute_id), INDEX IDX_F8DA03BF1A7833E2 (user_menu_item_id), PRIMARY KEY(atribute_id, user_menu_item_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE atribute_user_menu_item ADD CONSTRAINT FK_F8DA03BFCAE21197 FOREIGN KEY (atribute_id) REFERENCES atribute (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE atribute_user_menu_item ADD CONSTRAINT FK_F8DA03BF1A7833E2 FOREIGN KEY (user_menu_item_id) REFERENCES user_menu_item (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE atribute_user_menu_item DROP FOREIGN KEY FK_F8DA03BFCAE21197');
        $this->addSql('DROP TABLE atribute');
        $this->addSql('DROP TABLE atribute_user_menu_item');
    }
}
