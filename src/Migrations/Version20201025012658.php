<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201025012658 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE consumer ADD sponsor_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE consumer ADD CONSTRAINT FK_705B372712F7FB51 FOREIGN KEY (sponsor_id) REFERENCES consumer (id)');
        $this->addSql('CREATE INDEX IDX_705B372712F7FB51 ON consumer (sponsor_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE consumer DROP FOREIGN KEY FK_705B372712F7FB51');
        $this->addSql('DROP INDEX IDX_705B372712F7FB51 ON consumer');
        $this->addSql('ALTER TABLE consumer DROP sponsor_id');
    }
}
