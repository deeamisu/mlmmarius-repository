<?php
namespace App\Controller;

use App\Entity\Cart;
//use App\Entity\Category;
use App\Entity\Comanda;
use App\Entity\PaymentLog;
use App\Entity\User;
use App\Entity\UserMenuItem;
use App\Form\ModalRegistrationUserType;
use App\Service\GeneralService;
use birkof\NetopiaMobilPay\Configuration\NetopiaMobilPayConfiguration;
use birkof\NetopiaMobilPay\Service\NetopiaMobilPayServiceInterface;
use Mobilpay\Payment\Request\RequestAbstract;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class PaymentController extends AbstractController
{
    /**
     * @Route("/pay/{id}", name="frontend_payment", methods={"GET"}, requirements={"id"="\d+"})
     * @param Cart $cart
     * @param NetopiaMobilPayServiceInterface $mobilPayService
     * @param GeneralService $generalService
     * @return Response
     */
    public function payAction(
        Cart $cart,
        NetopiaMobilPayServiceInterface $mobilPayService,
        GeneralService $generalService
    ): Response
    {

        $consumer = new User();
        $form = $this->createForm(ModalRegistrationUserType::class, $consumer);
        $qrCode = $generalService->getQrCodeRegistrationLink();
        $menuItems = $this->getDoctrine()->getManager()->getRepository(UserMenuItem::class)->findAll();




        if (!is_object($cart) && $cart->getPaymentStatus() != '') {
            throw new NotFoundHttpException('comanda nu a fost gasita!');
        }

        try {
            $user = $cart->getUser();
            $address = [
                'type'           => 'person', // 'person' or 'company'
                'firstName'      => $user->getName(),
                'lastName'       => $user->getSurname(),
                'fiscalNumber'   => null,
                'identityNumber' => null,
                'country'        => 'RO',
                'county'         => $user->getCounty()->getName(),
                'city'           => $user->getCity(),
                'zipCode'        => null,
                'address'        => $user->getAdress(),
                'email'          => $user->getMail(),
                'mobilePhone'    => $user->getPhone(),
                'bank'           => null,
                'iban'           => null,
            ];


            $objPmReqCard = $mobilPayService->createCreditCardPaymentObject(
                $cart->getId(),
                $cart->getPrice(),
                NetopiaMobilPayConfiguration::CURRENCY_RON,
                '',
                $address,
                $address
            );


        } catch (\Exception $exception) {
            return $this->render('payload/plateste_error.html.twig', [
                'message' => $exception->getMessage(),
                'menuItems' => $menuItems,
                'form' => $form->createView(),
                'qrCode' => $qrCode,
            ]);
        }

        dump($mobilPayService->getMobilPayConfiguration());

        return $this->render('payload/plateste.html.twig', [
            'objPmReqCard' => $objPmReqCard,
            'payment_url'   => $mobilPayService->getMobilPayConfiguration()->getPaymentUrl(),
            //'categories' => $this->getDoctrine()->getRepository(Category::class)->findAll(),
            'menuItems' => $menuItems,
            'form' => $form->createView(),
            'qrCode' => $qrCode,
        ]);
    }

    /**
     * @Route("/netopia-mobilpay/thank-you", name="netopia_mobilpay_return_url", methods={"GET"})
     * @param Request $request
     * @return Response
     */
    public function platesteFinal(Request $request): Response
    {
        $orderId = $request->query->get('orderId');
        $order = $this->getDoctrine()->getRepository(Cart::class)->find($orderId);
        if ($order === null) {
            throw new NotFoundHttpException('Order not found!');
        }

        return $this->render('payload/plateste-final.html.twig', [
            'categories' => $this->getDoctrine()->getRepository(Category::class)->findAll(),
            'comanda' => $order

        ]);
    }

    /**
     * @Route("/payment/return_url", name="netopia_mobilpay_confirm_url", methods={"POST|GET"})
     */
    public function updateStatus(Request $request, ParameterBagInterface $parameterBag)
    {
        $method = $request->getRealMethod();
        $errorCode 		= 0;
        $errorType		= RequestAbstract::CONFIRM_ERROR_TYPE_NONE;
        $errorMessage	= '';

        if (strcasecmp($method, 'post') == 0)
        {
            $params = $request->request->all();
            if (isset($params['env_key']) && isset($params['data'])) {
                try {
                    $privateKeyFilePath = $parameterBag->get('kernel.project_dir') . $this->getParameter('NETOPIA_MOBILPAY_PRIVATE_KEY');
//                    var_dump($params);die;
                    $objPmReq = RequestAbstract::factoryFromEncrypted($params['env_key'], $params['data'], $privateKeyFilePath);

                    /** @var Cart $order */
                    $order = $this->getDoctrine()->getManager()->getRepository(Cart::class)->find($objPmReq->orderId);

                    $dbLog = new PaymentLog();
                    $dbLog->setOrder($order);
                    $dbLog->setLog(json_encode($objPmReq));
                    $this->getDoctrine()->getManager()->persist($dbLog);
                    $this->getDoctrine()->getManager()->flush($dbLog);

                    $rrn = $objPmReq->objPmNotify->rrn;
                    if ($objPmReq->objPmNotify->errorCode == 0) {
                        switch($objPmReq->objPmNotify->action)
                        {
                            case 'confirmed':
                                #cand action este confirmed avem certitudinea ca banii au plecat din contul posesorului de card si facem update al starii comenzii si livrarea produsului
                                //update DB, SET status = "confirmed/captured"
                                $order->setPaymentStatus('confirmed');
                                $errorMessage = $objPmReq->objPmNotify->errorMessage;
                                break;
                            case 'confirmed_pending':
                                #cand action este confirmed_pending inseamna ca tranzactia este in curs de verificare antifrauda. Nu facem livrare/expediere. In urma trecerii de aceasta verificare se va primi o noua notificare pentru o actiune de confirmare sau anulare.
                                //update DB, SET status = "pending"
                                $order->setPaymentStatus('pending');
                                $errorMessage = $objPmReq->objPmNotify->errorMessage;
                                break;
                            case 'paid_pending':
                                #cand action este paid_pending inseamna ca tranzactia este in curs de verificare. Nu facem livrare/expediere. In urma trecerii de aceasta verificare se va primi o noua notificare pentru o actiune de confirmare sau anulare.
                                //update DB, SET status = "pending"
                                $order->setPaymentStatus('pending');
                                $errorMessage = $objPmReq->objPmNotify->errorMessage;
                                break;
                            case 'paid':
                                #cand action este paid inseamna ca tranzactia este in curs de procesare. Nu facem livrare/expediere. In urma trecerii de aceasta procesare se va primi o noua notificare pentru o actiune de confirmare sau anulare.
                                //update DB, SET status = "open/preauthorized"
                                $order->setPaymentStatus('preauthorized');
                                $errorMessage = $objPmReq->objPmNotify->errorMessage;
                                break;
                            case 'canceled':
                                #cand action este canceled inseamna ca tranzactia este anulata. Nu facem livrare/expediere.
                                //update DB, SET status = "canceled"
                                $order->setPaymentStatus('canceled');
                                $errorMessage = $objPmReq->objPmNotify->errorMessage;
                                break;
                            case 'credit':
                                #cand action este credit inseamna ca banii sunt returnati posesorului de card. Daca s-a facut deja livrare, aceasta trebuie oprita sau facut un reverse.
                                //update DB, SET status = "refunded"
                                $order->setPaymentStatus('refunded');
                                $errorMessage = $objPmReq->objPmNotify->errorMessage;
                                break;
                            default:
                                $errorType		= RequestAbstract::CONFIRM_ERROR_TYPE_PERMANENT;
                                $errorCode 		= RequestAbstract::ERROR_CONFIRM_INVALID_ACTION;
                                $errorMessage 	= 'mobilpay_refference_action paramaters is invalid';
                                break;
                        }
                        $this->getDoctrine()->getManager()->persist($order);
                        $this->getDoctrine()->getManager()->flush();
                    } else {
                        //update DB, SET status = "rejected"
                        $order->setPaymentStatus('rejected');
                        $this->getDoctrine()->getManager()->persist($order);
                        $this->getDoctrine()->getManager()->flush();
                        $errorMessage = $objPmReq->objPmNotify->errorMessage;
                    }
                } catch (\Exception $exception) {
                    $errorType 		= RequestAbstract::CONFIRM_ERROR_TYPE_TEMPORARY;
                    $errorCode		= $exception->getCode();
                    $errorMessage 	= $exception->getMessage();
                    var_dump($errorMessage);die;
                }
            } else {
                $errorType 		= RequestAbstract::CONFIRM_ERROR_TYPE_PERMANENT;
                $errorCode		= RequestAbstract::ERROR_CONFIRM_INVALID_POST_PARAMETERS;
                $errorMessage 	= 'mobilpay.ro posted invalid parameters';
            }

        } else {
            $errorType 		= RequestAbstract::CONFIRM_ERROR_TYPE_PERMANENT;
            $errorCode		= RequestAbstract::ERROR_CONFIRM_INVALID_POST_METHOD;
            $errorMessage 	= 'invalid request metod for payment confirmation';
        }


        if($errorCode == 0)
        {
            $xml = "<crc>{$errorMessage}</crc>";
        }
        else
        {
            $xml = "<crc error_type=\"{$errorType}\" error_code=\"{$errorCode}\">{$errorMessage}</crc>";
        }

        $response = new Response($xml);
        $response->headers->set('Content-Type', 'xml');

        return $response;
    }
}
