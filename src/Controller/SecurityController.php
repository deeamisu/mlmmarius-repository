<?php

namespace App\Controller;

use App\Entity\User;
use App\FormType\ChangePasswordFormType;
use App\FormType\ForgottenUserType;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
         if ($this->getUser()) {
             return $this->redirectToRoute('dashboard');
         }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/forgot", name="app_forgot")
     */
    public function forgot(Request $request, \Swift_Mailer $mailer)
    {

        $tmpUser = new User();
        $entityManager = $this->getDoctrine()->getManager();

        $form = $this->createForm(ForgottenUserType::class, $tmpUser);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var User $user */
            $user = $entityManager->getRepository(User::class)->findOneBy(['username' => $tmpUser->getUsername()]);

            if ($user) {
                if (null === $user->getConfirmationToken()) {
                    /** @var $tokenGenerator TokenGeneratorInterface */
                    $token = md5(uniqid($user->getUsername(), true));
                    $user->setConfirmationToken($token);
                    $user->setPasswordRequestedAt(new \DateTime());
                    $entityManager->persist($user);

                    $message = (new \Swift_Message('Resetare parola'))
                        ->setFrom('noreply@onbook.ro')
                        ->setTo($user->getUsername())
                        ->setBody(
                            $this->render('security/email/reset_password.html.twig', ['token' => $token]),
                            'text/html'
                        );
                    $mailer->send($message);

                    $entityManager->flush();
                }
            }
            $this->addFlash('info', 'Daca userul a fost gasit veti primi instructiunile de resetare pe email.');
        }

        return $this->render("security/forgot.html.twig", array("form" => $form->createView()));
    }

    /**
     * Reset user password.
     * @Route("/reset-password/{token}", name="app_reset")
     * @param Request $request
     * @param $token
     */
    public function resetPassword(Request $request, $token, UserPasswordEncoderInterface $passwordEncoder)
    {
        $entityManager = $this->getDoctrine()->getManager();
        /** @var User $user */
        $user = $entityManager->getRepository(User::class)->findOneBy(['confirmationToken' => $token]);

        if (null === $user) {
            return new RedirectResponse($this->generateUrl('app_forgot'));
        }

        $form = $this->createForm(ChangePasswordFormType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->SetConfirmationToken(null);
            $user->setPasswordRequestedAt(null);
            $password = $passwordEncoder->encodePassword($user, $request->request->get('change_password_form')['plainPassword']['first']);
            $user->setPassword($password);
            $entityManager->persist($user);
            $entityManager->flush();

            $this->addFlash("success", "Parola ta a fost resetata cu succes. Te poti loga acum.");
            $url = $this->generateUrl('app_login'); //route to login page
            $response = new RedirectResponse($url);

            return $response;
        }

        return $this->render('security/reset.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }
}
