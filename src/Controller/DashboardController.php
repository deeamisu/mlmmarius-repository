<?php

namespace App\Controller;

use App\Entity\Role;
use App\Entity\Status;
use App\Entity\User;
use App\Event\RegistrationConfirmationEvent;
use App\EventSubscriber\RegistrationConfirmationSubscriber;
use App\Form\AccountDetailsType;
use App\Entity\UserMenuItem;
use App\Form\ModalRegistrationUserType;
use App\Form\UserType;
use App\Service\BonusService;
use App\Service\GeneralService;
use App\Service\ProfilePhotoUploadService;
use OldSound\RabbitMqBundle\RabbitMq\ProducerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class DashboardController extends AbstractController
{
    /**
     * @Route("/", name="dashboard")
     */
    public function index(GeneralService $generalService, BonusService $bonusService)
    {
        $consumer = new User();
        $form = $this->createForm(ModalRegistrationUserType::class, $consumer);

        $qrCode = $generalService->getQrCodeRegistrationLink();
        $stars = $generalService->getStars();
        $totalMembers = $generalService->countClients($this->getUser(), 0);
        $currentMonth = $generalService->getMonth();
        $menuItems = $this->getDoctrine()->getManager()->getRepository(UserMenuItem::class)->findAll();
        $monthlyBonus = $bonusService->getMonthlyBonus($bonusService->setBonusCircles($this->getUser()), $this->getUser());
        $totalBonus = $bonusService->getTotalBonus($this->getUser());

        return $this->render('dashboard/index.html.twig', [
            'menuItems' => $menuItems,
            'form' => $form->createView(),
            'stars' => $stars,
            'totalMembers' => $totalMembers,
            'currentMonth' => $currentMonth,
            'qrCode' => $qrCode,
            'monthlyBonus' => $monthlyBonus,
            'totalBonus' => $totalBonus,
        ]);
    }

    /**
     * @Route("/new/costumer", name="new_consumer", methods={"POST"})
     */
    public function newConsumer(Request $request, GeneralService $generalService, MailerInterface $mailer2)
    {
        /**
         * @var $consumer User
         */
        $consumer = new User();
        $form = $this->createForm(ModalRegistrationUserType::class,$consumer);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $email = (new TemplatedEmail())
                ->from('noreply@echipavultur.ro')
                ->to(new Address($form->get('mail')->getData()))
                ->subject('Inscrie-te in Echipa Vultur')
                // path of the Twig template to render
                ->htmlTemplate('dashboard/reg_form.html.twig')
                //pass variables (name => value) to the template
                ->context([
                    'link' => $generalService->getRegistrationLink(),
                ])
            ;
            $mailer2->send($email);
        }
        elseif ($form->isSubmitted() && !($form->isValid())) {
            $errors = $form['mail']->getErrors();
            return $this->render('dashboard/registration_email_error.html.twig', [
                'errors' => $errors
            ]);
        }
        return $this->redirectToRoute('dashboard');
    }

    /**
     * @Route("/network", name="network")
     */
    public function network(GeneralService $generalService)
    {
        $consumer = new User();
        $form = $this->createForm(ModalRegistrationUserType::class,$consumer);

        $circle = $generalService->setCircle();
        $table = $generalService->setNetworkTable($circle);

        $qrCode = $generalService->getQrCodeRegistrationLink();
        $menuItems = $this->getDoctrine()->getManager()->getRepository(UserMenuItem::class)->findAll();
        return $this->render('dashboard/network.html.twig', [
            'menuItems' => $menuItems,
            'form' => $form->createView(),
            'circles' => $circle,
            'table' => $table,
            'qrCode' => $qrCode,
            'user' => $this->getUser()
        ]);
    }

    /**
     * @Route("/registration/{id}", name="registration_form")
     */
    public function registrationForm(User $user, Request $request, GeneralService $generalService)
    {
        $consumer = new User();
        $form = $this->createForm(UserType::class,$consumer);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->get('acordGDPR')->getData() && $form->get('terms')->getData()){
                $consumer->setUsername($consumer->getMail());
                $password = rand(100000,999999);
                $consumer->setPassword($generalService->encodePassword($consumer, $password));
                $consumer->setSponsor($user);



                $entityManager = $this->getDoctrine()->getManager();
                $role = $entityManager->getRepository(Role::class)->findOneBy(['name' => 'ROLE_USER']);
                $consumer->addUserRole($role);

                $status = $entityManager->getRepository(Status::class)->findOneBy(['name' => 'Activ']);
                $consumer->setStatus($status);

                $entityManager->persist($consumer);
                $entityManager->flush();

                $dispatcher = new EventDispatcher();
                $subscriber = new RegistrationConfirmationSubscriber($generalService);
                $dispatcher->addSubscriber($subscriber);
                $myEvent = new RegistrationConfirmationEvent($consumer, $password);
                $dispatcher->dispatch($myEvent, RegistrationConfirmationEvent::NAME);
                return $this->redirectToRoute('dashboard');
            }
        }
        elseif ($form->isSubmitted() && !($form->isValid())) {
            $errors = $form['mail']->getErrors();
            return $this->render('dashboard/registration_email_error.html.twig', [
                'errors' => $errors
            ]);
        }

        return $this->render('dashboard/registration_form.html.twig', [
            'form' => $form->createView(),
            'id' => $user->getId(),
        ]);

    }

    /**
     * @Route("/terms_conditions", name="terms_conditions")
     */
    public function terms_conditions()
    {
        return $this->render('dashboard/terms_conditions.html.twig');
    }

    /**
     * @Route("/edit", name="account_details", methods={"POST","GET"})
     * @param Request $request
     * @param GeneralService $generalService
     * @param ProfilePhotoUploadService $photoUploadService
     * @return RedirectResponse|Response
     */
    public function accountDetails(Request $request, GeneralService $generalService, ProfilePhotoUploadService  $photoUploadService)
    {
        /**
         * @var $userDetails User
         * @var $user User
         */
        $userDetails = new User();
        $user = $this->getUser();
        $form = $this->createForm(ModalRegistrationUserType::class,$userDetails);

        $form1 = $this->createForm(AccountDetailsType::class,$userDetails, [
            'allow_file_upload' => true,
        ]);
        $form1->handleRequest($request);
        if ($form1->isSubmitted() && $form1->isValid()) {
            if ($form1->get('password')->getData() != $form1->get('password2')->getData()) {
                $form1->addError(new FormError('Parola introdusa gresit',null,[],null,null));
            }
            else {
                $entityManager = $this->getDoctrine()->getManager();
                $mail = $form1->get('mail')->getData();
                $password = $form1->get('password')->getData();
                $phone = $form1->get('phone')->getData();
                /** @var UploadedFile $photo */
                $photo = $form1->get('ph')->getData();
                $modified = false;

                if ($mail != '') {
                    $user->setMail($mail);
                    $modified = true;
                }
                if ($phone != '') {
                    $user->setPhone($phone);
                    $modified = true;
                }
                if ($password != '') {
                    $user->setPassword($generalService->encodePassword($user,$password));
                    $modified = true;
                }
                if ($photo) {
                    $user = $photoUploadService->uploadPhoto($user,$photo,$request);
                    $modified = true;
                }
                if ($modified) {
                    $entityManager->flush();
                    return $this->redirectToRoute('dashboard');
                }
            }
        }
        $menuItems = $this->getDoctrine()->getManager()->getRepository(UserMenuItem::class)->findAll();
        $qrCode = $generalService->getQrCodeRegistrationLink();
        return $this->render('dashboard/account_details.html.twig', [
            'form1' => $form1->createView(),
            'form' => $form->createView(),
            'menuItems' => $menuItems,
            'qrCode' => $qrCode,
        ]);
    }

}