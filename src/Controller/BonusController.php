<?php

namespace App\Controller;

use App\Entity\Bonus;
use App\Entity\HistoryBonus;
use App\Entity\Subscription;
use App\Entity\User;
use App\Entity\UserMenuItem;
use App\Form\ModalRegistrationUserType;
use App\Service\BonusService;
use App\Service\GeneralService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Extra\Intl\IntlExtension;

class BonusController extends AbstractController
{
    /**
     * @Route("/bonus", name="bonus")
     */
    public function index(BonusService $bonusService)
    {
        $consumer = new User();
        $form = $this->createForm(ModalRegistrationUserType::class, $consumer);

        $qrCode = $bonusService->getQrCodeRegistrationLink();
        $currentMonth = $bonusService->getMonth();
        $menuItems = $this->getDoctrine()->getManager()->getRepository(UserMenuItem::class)->findAll();

        /** @var User $user */
        $user = $this->getUser();
        $bonuses = $this->getDoctrine()->getRepository(Bonus::class)->findAll();
        $circles = $bonusService->setBonusCircles($user);
        $subscription = $this->getDoctrine()->getRepository(Subscription::class)->findOneBy([
            'name' => 'Abonament'
        ]);
        $monthlyBonus = $bonusService->getMonthlyBonus($circles, $user);
        $historyBonuses = $user->getHistoryBonuses();

        //$this->get('twig')->addExtension(new IntlExtension());
        //$twig = $this->get('twig');
        //$twig->addExtension(new IntlExtension());

        return $this->render('bonus/index.html.twig', [
            'menuItems' => $menuItems,
            'currentMonth' => $currentMonth,
            'form' => $form->createView(),
            'qrCode' => $qrCode,
            'bonuses' => $bonuses,
            'circles' => $circles,
            'subscription' => $subscription,
            'monthlyBonus' => $monthlyBonus,
            'historyBonuses' => $historyBonuses,
        ]);
    }

    /**
     * @Route("/bonus/registry", name="bouns_registry")
     * @param GeneralService $generalService
     * @return Response
     */
    public function bonusTableAction(GeneralService $generalService): Response
    {
        $consumer = new User();
        $form = $this->createForm(ModalRegistrationUserType::class, $consumer);
        $qrCode = $generalService->getQrCodeRegistrationLink();
        $menuItems = $this->getDoctrine()->getManager()->getRepository(UserMenuItem::class)->findAll();

        $user = $this->getUser();
        $registry = $this->getDoctrine()->getRepository(HistoryBonus::class)->findBy(['user' => $user]);

        return $this->render('bonus/bonus_log.html.twig', [
            'menuItems' => $menuItems,
            'form' => $form->createView(),
            'qrCode' => $qrCode,
            'registry' => $registry
        ]);
    }
}
