<?php

namespace App\Controller;

use App\Entity\Cart;
use App\Entity\CartItem;
use App\Entity\Product;
use App\Entity\User;
use App\Entity\UserMenuItem;
use App\Form\ModalRegistrationUserType;
use App\Service\GeneralService;
use App\Service\OrderService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ProductsController
 * @Route("/products")
 * @package App\Controller
 */
class ProductsController extends AbstractController
{
    /**
     * @Route("/list", name="products")
     * @param GeneralService $generalService
     * @return Response
     */
    public function listAction(GeneralService $generalService): Response
    {
        $consumer = new User();
        $form = $this->createForm(ModalRegistrationUserType::class, $consumer);
        $qrCode = $generalService->getQrCodeRegistrationLink();
        $products = $this->getDoctrine()->getRepository(Product::class)->findAll();

        $menuItems = $this->getDoctrine()->getManager()->getRepository(UserMenuItem::class)->findAll();
        return $this->render('products/products.html.twig', [
            'menuItems' => $menuItems,
            'form' => $form->createView(),
            'qrCode' => $qrCode,
            'products' => $products
        ]);
    }

    /**
     * @Route("/product/{id}", name="product.details")
     * @param int $id
     * @param GeneralService $generalService
     * @return Response
     */
    public function listProductAction(int $id, GeneralService $generalService): Response
    {
        $product = $this->getDoctrine()->getManager()->find(Product::class, $id);

        $consumer = new User();
        $form = $this->createForm(ModalRegistrationUserType::class, $consumer);
        $qrCode = $generalService->getQrCodeRegistrationLink();
        $menuItems = $this->getDoctrine()->getManager()->getRepository(UserMenuItem::class)->findAll();

        return $this->render('products/product_detail.html.twig', [
            'menuItems' => $menuItems,
            'form' => $form->createView(),
            'qrCode' => $qrCode,
            'product' => $product
        ]);
    }

    /**
     * @Route("/cart", name="products.cart")
     * @param GeneralService $generalService
     * @return Response
     */
    public function listCartItems(GeneralService $generalService): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $cart = $user->getCart();

        $consumer = new User();
        $form = $this->createForm(ModalRegistrationUserType::class, $consumer);
        $qrCode = $generalService->getQrCodeRegistrationLink();
        $menuItems = $this->getDoctrine()->getManager()->getRepository(UserMenuItem::class)->findAll();

        return $this->render('products/cart.html.twig', [
            'menuItems' => $menuItems,
            'form' => $form->createView(),
            'qrCode' => $qrCode,
            'cart' => $cart
        ]);
    }

    /**
     * @Route("/cart/add/{id}", name="add.product.to.cart")
     * @param Product $product
     * @param Request $request
     * @return RedirectResponse
     */
    public function addToCartAction(Product $product, Request $request): RedirectResponse
    {
        /** @var User $user */
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        if (!$user->getCart()) {
            $cart = new Cart();
            $cart->setUser($user);
        } else {
            $cart = $user->getCart();
            $em->persist($cart);
        }

        $quantity = $request->request->get('quantity');

        $cartItem = new CartItem();
        $cartItem->setProduct($product);
        $cartItem->setCart($cart);
        $cartItem->setQuantity($quantity);

        $em->persist($cartItem);
        $em->flush();
        $this->addFlash('succes', "Produs adaugat in cosul de cumparaturi");

        return $this->redirectToRoute('product.details', [
            'id' => $product->getId()
        ]);
    }

    /**
     * @Route("/remove/cartItem/{id}", name="remove_cartItem")
     * @param CartItem $cartItem
     * @param OrderService $orderService
     * @return RedirectResponse
     */
    public function removeFromItemCartAction(CartItem $cartItem, OrderService $orderService): RedirectResponse
    {
        $orderService->removeCartItem($cartItem);
        return $this->redirectToRoute("products.cart");
    }
}
