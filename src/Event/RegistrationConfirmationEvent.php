<?php
/**
 * Created by PhpStorm.
 * User: Misu
 * Date: 11/11/2020
 * Time: 1:28 AM
 */

namespace App\Event;

use App\Entity\User;
use Symfony\Contracts\EventDispatcher\Event;



class RegistrationConfirmationEvent extends Event
{
    public const NAME = 'registration.confirmed';

    protected $consumer;

    protected $password;

    /**
     * RegistrationConfirmationEvent constructor.
     * @param User $consumer
     * @param $password
     */
    public function __construct(User $consumer, $password)
    {
        $this->consumer = $consumer;
        $this->password = $password;
    }

    /**
     * @return User
     */
    public function getConsumer()
    {
        return $this->consumer;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }



}